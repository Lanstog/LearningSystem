package Windows;

import Controllers.Controller;

import javax.swing.*;
import java.awt.event.*;

public class AuthorizationWindow extends JDialog {
    private JPanel contentPane;
    private JTextField logInField;
    private JButton okButton;
    private JButton registrationButton;
    private JPasswordField passwordField;

    public AuthorizationWindow(Controller controller) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(okButton);
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.authorizationPressed(logInField.getText(), new String(passwordField.getPassword()));
            }
        });
        registrationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                controller.registrationPressed();
            }
        });
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.pack();
        this.setLocationRelativeTo(null);
    }
}
