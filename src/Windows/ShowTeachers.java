package Windows;

import Models.Book;
import Models.DataBaseModel;
import Models.Teacher;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ShowTeachers extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTable teacherTable;
    private JScrollPane jScroll;
    private ArrayList<Teacher> teachers;

    public ShowTeachers() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        teachers=DataBaseModel.getTeachers();
        teacherTable=new JTable(defTableModel(teachers)){
            private static final long serialVersionUID = 1L;
            @Override
            public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return Boolean.class;
                    default:
                        return String.class;
                }
            }
        };
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(int i=0;i<teacherTable.getRowCount();i++){
                    if((boolean)teacherTable.getModel().getValueAt(i,0)){
                        Teacher teacher=new Teacher();
                        teacher.setLastName((String)teacherTable.getModel().getValueAt(i, 1));
                        teacher.setFirstName((String)teacherTable.getModel().getValueAt(i, 2));
                        teacher.setPatronymic((String)teacherTable.getModel().getValueAt(i, 3));
                        teacher.setPost((String)teacherTable.getModel().getValueAt(i, 4));
                        for(int j=0;j<teachers.size();j++){
                            if(teacher.getLastName().equals(teachers.get(j).getLastName())&&teacher.getFirstName().equals(teachers.get(j).getFirstName())&&teacher.getPatronymic().equals(teachers.get(j).getPatronymic())){
                                teacher.setId(teachers.get(j).getId());
                                break;
                            }
                        }
                        DataBaseModel.acceptTeacher(teacher);
                    }
                }
                JOptionPane.showMessageDialog(null, "Все выбранные преподаватели подключены к системе!");
                setVisible(false);
            }
        });
        jScroll.getViewport().add(teacherTable);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private DefaultTableModel defTableModel(ArrayList<Teacher> teachers){
        Object [] titles={"", "Фамилия", "Имя", "Отчество", "Должность"};
        ArrayList<Object> objects=new ArrayList<>();
        for(int i=0;i<teachers.size();i++){
            Object[] object={false, teachers.get(i).getLastName(), teachers.get(i).getFirstName(), teachers.get(i).getPatronymic(), teachers.get(i).getPost()};
            objects.add(object);
        }
        Object [][]values=objects.toArray(new Object[objects.size()][]);
        return new DefaultTableModel(values, titles);
    }
}
