package Windows;

import Controllers.Controller;
import Controllers.UserController;
import Models.Student;

import javax.swing.*;
import java.awt.event.*;

public class UserWindow extends JDialog {
    private JPanel contentPane;
    private JButton yourBooksButton;
    private JButton addNewBookButton;
    private JButton recommendedBooksButton;
    private JButton deleteBookButton;
    private JButton editBookButton;
    private JButton showUserInfoButton;

    public UserWindow(Object user, Controller cont) {
        UserWindow userWindow =this;
        setContentPane(contentPane);
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        addNewBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserController.createNewBook(user);
            }
        });
        showUserInfoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserController.showUserInfo(user);
            }
        });
        yourBooksButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserController.showAllBooks(user);
            }
        });
        recommendedBooksButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserController.showRecomendedBooks(user);
            }
        });
        deleteBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserController.deleteBooks(user);
            }
        });
        editBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UserController.changeBooks(user);
            }
        });
        setVisible(true);
    }
}
