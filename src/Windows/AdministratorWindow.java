package Windows;

import Controllers.AdministratorController;
import Controllers.UserController;

import javax.swing.*;
import java.awt.event.*;

public class AdministratorWindow extends JDialog {
    private JPanel contentPane;
    private JButton editBookButton;
    private JButton deleteBookButton;
    private JButton showTeacherAppButton;

    public AdministratorWindow() {
        setContentPane(contentPane);
        setModal(true);
        pack();
        setLocationRelativeTo(null);
        deleteBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AdministratorController.deleteBooks();
            }
        });
        editBookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AdministratorController.changeBooks();
            }
        });
        showTeacherAppButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AdministratorController.showTeachers();
            }
        });
        setVisible(true);
    }
}
