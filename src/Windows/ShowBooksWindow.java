package Windows;

import Models.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ShowBooksWindow extends JDialog {
    private JPanel contentPane;
    private JTable booksTable;
    private JTextField subjectField;
    private JLabel subjectLabel;
    private JButton findBooks;
    private JScrollPane jScroll;
    private JPanel jPanel;
    private JButton deleteButton;
    private JPanel deletePanel;
    private ArrayList<Book> books;
    private ShowBooksWindow showBooksWindow;
    private Object user;
    private Book book;

    public ShowBooksWindow(ArrayList<Book> books, int mode, Object user) {
        setContentPane(contentPane);
        showBooksWindow=this;
        this.books=books;
        this.user=user;
        setModal(true);
        if(mode==1) {
            setTitle("Просмотр всех учебников");
            booksTable.setModel(new TableModel(this.books));
            booksTable.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getClickCount() == 2) {
                        Desktop desktop = null;
                        if (Desktop.isDesktopSupported()) {
                            desktop = Desktop.getDesktop();
                        }
                        try {
                            desktop.open(new File(selectBook(booksTable.rowAtPoint(e.getPoint())).getURL()));
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                            JOptionPane.showMessageDialog(null, "Не удается открыть файл!");
                        }
                    }
                }
            });
            deletePanel.setVisible(false);
        }
        if(mode==2){
            setTitle("Удаление учебника");
            booksTable=new JTable(deleteTableModel(books)) {
                private static final long serialVersionUID = 1L;
                @Override
                public Class getColumnClass(int column) {
                    switch (column) {
                        case 0:
                            return Boolean.class;
                        default:
                            return String.class;
                    }
                }
            };
            jScroll.getViewport().add(booksTable);
            deleteButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    for(int i=0;i<booksTable.getRowCount();i++){
                        if((boolean)booksTable.getModel().getValueAt(i, 0)){
                            Book book=new Book();
                            book.setTitle((String)booksTable.getModel().getValueAt(i, 1));
                            book.setAuthor((String)booksTable.getModel().getValueAt(i, 2));
                            book.setSubject((String)booksTable.getModel().getValueAt(i, 3));
                            book.setThemes((String)booksTable.getModel().getValueAt(i, 4));
                            book.setURL((String)booksTable.getModel().getValueAt(i, 5));
                            if(user instanceof Student){
                                DataBaseModel.deleteBook(((Student) user).getId(), book);
                            }
                            if(user instanceof Teacher){
                                DataBaseModel.deleteBook(((Teacher)user).getId(), book);
                            }
                            if(user==null){
                                DataBaseModel.deleteBook(0, book);
                            }
                        }
                    }
                    JOptionPane.showMessageDialog(null, "Учебники были удалены!");
                    setVisible(false);
                }
            });
        }
        if(mode==3){
            setTitle("Изменение учебника");
            booksTable.setModel(new TableModel(this.books));
            booksTable.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    doubleClick(e);
                }
            });
            deletePanel.setVisible(false);
        }
        pack();
        setLocationRelativeTo(null);
        subjectField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findBooks(mode);
            }
        });
        findBooks.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                findBooks(mode);
            }
        });
        setVisible(true);
    }
    private void findBooks(int mode){
        if(mode==1 || mode==3) {
            if (subjectField.getText().isEmpty()) {
                booksTable.setModel(new TableModel(books));
            } else {
                ArrayList<Book> setBooks = new ArrayList<>();
                for (int i = 0; i < books.size(); i++) {
                    if (books.get(i).getSubject().contains(subjectField.getText())) {
                        setBooks.add(books.get(i));
                    }
                }
                booksTable.setModel(new TableModel(setBooks));
            }
        }
        if(mode==2){
            if (subjectField.getText().isEmpty()) {
                booksTable.setModel(deleteTableModel(books));
            } else {
                ArrayList<Book> setBooks = new ArrayList<>();
                for (int i = 0; i < books.size(); i++) {
                    if (books.get(i).getSubject().contains(subjectField.getText())) {
                        setBooks.add(books.get(i));
                    }
                }
                booksTable.setModel(deleteTableModel(setBooks));
            }
        }
    }

    private DefaultTableModel deleteTableModel(ArrayList<Book> books){
        Object [] titles={"", "Название", "Автор", "Предмет", "Темы", "URL"};
        ArrayList<Object> objects=new ArrayList<>();
        for(int i=0;i<books.size();i++){
            Object[] object={false, books.get(i).getTitle(), books.get(i).getAuthor(), books.get(i).getSubject(), books.get(i).getThemes(), books.get(i).getURL()};
            objects.add(object);
        }
        Object [][]values=objects.toArray(new Object[objects.size()][]);
        return new DefaultTableModel(values, titles);
    }

    public void changeConfirmed(){
        books.remove(this.book);
        if(user instanceof Student) {
            DataBaseModel.deleteBook(((Student) user).getId(), this.book);
            this.books=DataBaseModel.getAllBooks(((Student) user).getId());
        }
        if(user instanceof Teacher) {
            DataBaseModel.deleteBook(((Teacher) user).getId(), this.book);
            this.books=DataBaseModel.getAllBooks(((Teacher) user).getId());
        }
        if(user==null){
            DataBaseModel.deleteBook(0, this.book);
            this.books=DataBaseModel.getAllBooks(0);
        }
        findBooks(3);
        JOptionPane.showMessageDialog(null, "Учебники успешно изменены!");
    }
    private void doubleClick(MouseEvent e){
        if (e.getClickCount() == 2) {
            NewBookWindow newBookWindow=new NewBookWindow(user, 2);
            book=selectBook(booksTable.rowAtPoint(e.getPoint()));
            newBookWindow.setValues(book, showBooksWindow, DataBaseModel.findId(book));
            newBookWindow.setVisible(true);
        }
    }
    private Book selectBook(int numberOfRow){
        for(int i=0;i<books.size();i++){
            if(booksTable.getModel().getValueAt(numberOfRow, 0).equals(books.get(i).getTitle())&&
                    booksTable.getModel().getValueAt(numberOfRow, 1).equals(books.get(i).getAuthor())&&
                    booksTable.getModel().getValueAt(numberOfRow, 2).equals(books.get(i).getSubject())&&
                    booksTable.getModel().getValueAt(numberOfRow, 3).equals(books.get(i).getThemes())&&
                    booksTable.getModel().getValueAt(numberOfRow, 4).equals(books.get(i).getURL())){
                return books.get(i);
            }
        }
        return null;
    }
}
