package Windows;

import Models.Student;
import Models.Teacher;

import javax.swing.*;

public class UserInfoWindow extends JDialog {
    private JPanel contentPane;
    private JLabel bonusField;
    private JLabel lastName;
    private JLabel firstName;
    private JLabel patronymic;
    private JLabel status;
    private JLabel bonusFieldValue;

    public UserInfoWindow(Object user) {
        if(user instanceof Student){
            new UserInfoWindow((Student)user).setVisible(true);
        }
        else{
            new UserInfoWindow((Teacher)user).setVisible(true);
        }
    }
    public UserInfoWindow(Student student){
        setTitle("Информация о пользователе");
        setContentPane(contentPane);
        setModal(true);
        status.setText("Студент");
        lastName.setText(student.getLastName());
        firstName.setText(student.getFirstName());
        patronymic.setText(student.getPatronymic());
        bonusField.setText("Курс:");
        bonusFieldValue.setText(String.valueOf(student.getCourse()));
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public UserInfoWindow(Teacher teacher) {
        setTitle("Информация о пользователе");
        setContentPane(contentPane);
        setModal(true);
        status.setText("Преподаватель");
        lastName.setText(teacher.getLastName());
        firstName.setText(teacher.getFirstName());
        patronymic.setText(teacher.getPatronymic());
        bonusField.setText("Должность:");
        bonusFieldValue.setText(String.valueOf(teacher.getPost()));
        pack();
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}
