package Windows;

import Models.DataBaseModel;
import Models.Student;
import Models.Teacher;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RegistrationWindow extends JDialog {
    private JPanel contentPane;
    private JTextField logInField;
    private JTextField firstNameField;
    private JTextField patronymicField;
    private JButton registrationButton;
    private JButton cancelButton;
    private JRadioButton studentRB;
    private JRadioButton teacherRB;
    private JTextField lastNameField;
    private JPanel mainPanel;
    private JTextField positionField;
    private JTextField courseField;
    private JLabel courseLabel;
    private JLabel positionLabel;
    private JPasswordField passwordField;

    public RegistrationWindow(AuthorizationWindow authorizationWindow) {
        super(authorizationWindow, "Регистрация", true);
        RegistrationWindow rw=this;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(registrationButton);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        hideElements();
        studentRB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hideElements();
            }
        });
        teacherRB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hideElements();
            }
        });
        registrationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object user;
                if(studentRB.isSelected())
                {
                    user=new Student();
                    ((Student) user).setFirstName(firstNameField.getText());
                    ((Student) user).setLastName(lastNameField.getText());
                    ((Student) user).setPatronymic(patronymicField.getText());
                    ((Student) user).setCourse(Integer.parseInt(courseField.getText()));
                }
                else {
                    user=new Teacher();
                    ((Teacher) user).setFirstName(firstNameField.getText());
                    ((Teacher) user).setLastName(lastNameField.getText());
                    ((Teacher) user).setPatronymic(patronymicField.getText());
                    ((Teacher) user).setPost(positionField.getText());
                }
                DataBaseModel.registration(user, logInField.getText(), new String(passwordField.getPassword()));
                JOptionPane.showMessageDialog(null, "Регистрация прошла успешно!");
                rw.setVisible(false);
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                rw.setVisible(false);
            }
        });
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    private void hideElements()
    {
        if(studentRB.isSelected())
        {
            courseField.setVisible(true);
            courseLabel.setVisible(true);
            positionField.setVisible(false);
            positionLabel.setVisible(false);
        }
        else
        {
            courseField.setVisible(false);
            courseLabel.setVisible(false);
            positionLabel.setVisible(true);
            positionField.setVisible(true);
        }
        pack();
    }
}
