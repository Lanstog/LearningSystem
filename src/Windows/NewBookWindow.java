package Windows;

import Models.Book;
import Models.DataBaseModel;
import Models.Student;
import Models.Teacher;

import javax.swing.*;
import java.awt.event.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class NewBookWindow extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField bookNameTextField;
    private JTextField authorNameTextField;
    private JTextArea topicsTextArea;
    private JTextField filePathTextField;
    private JButton fileChooseButton;
    private JTextField subjectField;
    private Object user;
    private File file;

    public NewBookWindow(Object user, int mode) {
        setTitle("Добавление учебника");
        this.user=user;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        if (mode==1) {
            buttonOK.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    onOK(0);
                }
            });
        }
        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });
        fileChooseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                int ret = fileopen.showDialog(null, "Выбрать файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    file = fileopen.getSelectedFile();
                    String path=file.getAbsolutePath();
                    String [] arraypath=path.split("\\\\");
                    filePathTextField.setText(".\\src\\books\\"+arraypath[arraypath.length-1]);
                }
            }
        });
        pack();
        setLocationRelativeTo(null);
    }

    private void onOK(int id) {
        String path;
        if(file==null){
            file=new File(filePathTextField.getText());
        }
        path=file.getAbsolutePath();
        String [] arraypath=path.split("\\\\");
        File secFile=new File(".\\src\\books", arraypath[arraypath.length-1]);
        try {
            Files.copy(file.toPath(), secFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        catch (Exception ex){
            return;
        }
        Book book=new Book();
        book.setTitle(bookNameTextField.getText());
        book.setAuthor(authorNameTextField.getText());
        book.setSubject(subjectField.getText());
        book.setThemes(topicsTextArea.getText());
        book.setURL(filePathTextField.getText());
        if(user instanceof Student){
            DataBaseModel.addNewBook(book, ((Student) user).getId());
        }
        if(user instanceof Teacher) {
            DataBaseModel.addNewBook(book, ((Teacher)user).getId());
        }
        if(user==null){
            DataBaseModel.addNewBook(book, id);
        }
        this.setVisible(false);
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public void setValues(Book book, ShowBooksWindow showBooksWindow, int id){
        bookNameTextField.setText(book.getTitle());
        authorNameTextField.setText(book.getAuthor());
        subjectField.setText(book.getSubject());
        topicsTextArea.setText(book.getThemes());
        filePathTextField.setText(book.getURL());
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOK(id);
                showBooksWindow.changeConfirmed();
            }
        });
    }
}
