package Controllers;

import Models.Book;
import Models.DataBaseModel;
import Models.Student;
import Models.Teacher;
import Windows.ShowBooksWindow;
import Windows.ShowTeachers;

import java.util.ArrayList;

public abstract class AdministratorController {
    public static void deleteBooks(){
        ArrayList<Book> books=DataBaseModel.getAllBooks(0);
        ShowBooksWindow showBooksWindow=new ShowBooksWindow(books, 2, null);
    }
    public static void changeBooks(){
        ArrayList<Book> books=DataBaseModel.getAllBooks(0);
        ShowBooksWindow showBooksWindow=new ShowBooksWindow(books, 3, null);
    }
    public static void showTeachers(){
        ShowTeachers showTeachers=new ShowTeachers();
    }
}
