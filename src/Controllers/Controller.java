package Controllers;

import Models.DataBaseModel;
import Models.Student;
import Models.Teacher;
import Windows.*;

import javax.swing.*;

public class Controller {
    private static Controller cont;
    private AuthorizationWindow authorizationWindow;
    private Controller()
    {
        authorizationWindow=new AuthorizationWindow(this);
        authorizationWindow.setVisible(true);
    }
    public void authorizationPressed(String logIn, String password){
        Object user=DataBaseModel.authorization(logIn, password);
        if(user==null){
            JOptionPane.showMessageDialog(null, "Неверный логин или пароль!");
        }
        else {
            if(user.equals(3)){
                authorizationWindow.setVisible(false);
                AdministratorWindow administratorWindow=new AdministratorWindow();
            }
            else{
                authorizationWindow.setVisible(false);
                UserWindow userWindow=new UserWindow(user, this);
            }
        }
    }
    public static Controller NewController()
    {
        if(cont==null)
        {
            cont=new Controller();
        }
        return cont;
    }
    public void registrationPressed()
    {
        RegistrationWindow registrationWindow=new RegistrationWindow(authorizationWindow);
    }
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Controller cont=NewController();
                System.exit(0);
            }
        });
    }
}
