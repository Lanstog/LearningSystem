package Controllers;

import Models.Book;
import Models.DataBaseModel;
import Models.Student;
import Models.Teacher;
import Windows.NewBookWindow;
import Windows.ShowBooksWindow;
import Windows.UserWindow;
import Windows.UserInfoWindow;

import java.util.ArrayList;

public abstract class UserController {
    public static void showUserInfo(Object user){
        UserInfoWindow userInfoWindow=new UserInfoWindow(user);
    }
    public static void createNewBook(Object user){
        NewBookWindow newBookWindow=new NewBookWindow(user, 1);
        newBookWindow.setVisible(true);
    }
    public static void showAllBooks(Object user){
        ArrayList<Book> books;
        if(user instanceof Student) {
            books = DataBaseModel.getAllBooks(((Student) user).getId());
        }
        else {
            books = DataBaseModel.getAllBooks(((Teacher) user).getId());
        }
        ShowBooksWindow showBooksWindow=new ShowBooksWindow(books, 1, user);
    }
    public static void showRecomendedBooks(Object user){
        ArrayList<Book> books=DataBaseModel.getRecomendedBooks();
        ShowBooksWindow showBooksWindow=new ShowBooksWindow(books, 1, user);
    }

    public static void deleteBooks(Object user){
        ArrayList<Book> books;
        if(user instanceof Student) {
            books = DataBaseModel.getAllBooks(((Student) user).getId());
        }
        else {
            books = DataBaseModel.getAllBooks(((Teacher) user).getId());
        }
        ShowBooksWindow showBooksWindow=new ShowBooksWindow(books, 2, user);
    }
    public static void changeBooks(Object user){
        ArrayList<Book> books;
        if(user instanceof Student) {
            books = DataBaseModel.getAllBooks(((Student) user).getId());
        }
        else {
            books = DataBaseModel.getAllBooks(((Teacher) user).getId());
        }
        ShowBooksWindow showBooksWindow=new ShowBooksWindow(books, 3, user);
    }
}
