package Models;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class TableModel extends AbstractTableModel {
    public TableModel(ArrayList<Book> books) {
        super();
        this.books = books;
    }

    public int getRowCount() {
        return books.size();
    }
    public int getColumnCount() {
        return 5;
    }
    public Object getValueAt(int r, int c) {
        switch (c) {
            case 0:
                return books.get(r).getTitle();
            case 1:
                return books.get(r).getAuthor();
            case 2:
                return books.get(r).getSubject();
            case 3:
                return books.get(r).getThemes();
            case 4:
                return books.get(r).getURL();
            default:
                return "";
        }
    }

    ArrayList<Book> books;
    public String getColumnName(int c) {
        String result = "";
        switch (c) {
            case 0:
                result = "Название";
                break;
            case 1:
                result = "Автор";
                break;
            case 2:
                result = "Предмет";
                break;
            case 3:
                result = "Темы";
                break;
            case 4:
                result = "URL";
                break;
        }
        return result;
    }

}
