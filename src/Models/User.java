package Models;

abstract class User {
    private String lastName;
    private String firstName;
    private String patronymic;
    private int id;
    public void setLastName(String lastName)
    {
        this.lastName=lastName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }
    public String getLastName() {
        return lastName;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getPatronymic() {
        return patronymic;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
}
