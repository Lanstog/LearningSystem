package Models;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

public abstract class DataBaseModel {
    private static final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/StudLib";
    private static final String user = "postgres";
    private static final String password = "123";
    private static int countOfIteration=1;
    private static int id=1;
    private static Connection connectToDB() {
        //System.out.println("Testing connection to PostgreSQL JDBC");

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
            return null;
        }

        //System.out.println("PostgreSQL JDBC Driver successfully connected");
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(DB_URL, user, password);

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
            return null;
        }

        if (connection != null) {
            //System.out.println("You successfully connected to database now");
        } else {
            System.out.println("Failed to make connection to database");
        }
        return connection;
    }

    public static Object authorization(String logIn, String password){

        try{
            Connection connection=connectToDB();
            Statement stmt=connection.createStatement();
            ResultSet rs=stmt.executeQuery("select * from userdata where login='"+logIn+"' and pass='"+password+"' and confirmed=1 order by id desc limit 1");
            if(rs.next()==false){
                stmt.close();
                rs.close();
                connection.close();
                return null;
            }
            else {
                int id=rs.getInt("id");
                int status=rs.getInt("status");
                Statement stmt2=connection.createStatement();
                ResultSet rs2=null;
                if(status==1){
                    rs2=stmt2.executeQuery("select course from student where id="+id);
                    Student student=new Student();
                    student.setLastName(rs.getString("lastname"));
                    student.setFirstName(rs.getString("firstname"));
                    student.setPatronymic(rs.getString("patronymic"));
                    student.setId(id);
                    rs2.next();
                    student.setCourse(rs2.getInt("course"));
                    stmt.close();
                    rs.close();
                    stmt2.close();
                    rs2.close();
                    return student;
                }
                if(status==2){
                    rs2=stmt2.executeQuery("select post from teacher where id="+id);
                    Teacher teacher=new Teacher();
                    teacher.setLastName(rs.getString("lastname"));
                    teacher.setFirstName(rs.getString("firstname"));
                    teacher.setPatronymic(rs.getString("patronymic"));
                    teacher.setId(id);
                    rs2.next();
                    teacher.setPost(rs2.getString("post"));
                    stmt.close();
                    rs.close();
                    stmt2.close();
                    rs2.close();
                    return teacher;
                }
                if(status==3){
                    return 3;
                }
            }
        }
        catch (Exception e){
            System.out.println("Ошибка авторизации!");
            return null;
        }
        return null;
    }

    public static void registration(Object user, String login, String password){
        try {
            Connection connection = connectToDB();
            String sql = "INSERT INTO userdata (login, pass, status, lastname, firstname, patronymic, confirmed) values(?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, login);
            stmt.setString(2, password);
            if(user instanceof Student){
                stmt.setInt(3, 1);
                stmt.setString(4, ((Student)user).getLastName());
                stmt.setString(5, ((Student)user).getFirstName());
                stmt.setString(6, ((Student)user).getPatronymic());
                stmt.setInt(7, 1);
            }
            if(user instanceof Teacher){
                stmt.setInt(3, 2);
                stmt.setString(4, ((Teacher)user).getLastName());
                stmt.setString(5, ((Teacher)user).getFirstName());
                stmt.setString(6, ((Teacher)user).getPatronymic());
                stmt.setInt(7, 0);
            }
            stmt.executeUpdate();
            stmt.close();
            Statement stmt2 = connection.createStatement();
            ResultSet rs=stmt2.executeQuery("Select id from userdata where login='"+login+"' and pass='"+password+"' order by id desc limit 1");
            int id=0;
            if(rs.next()){
                id=rs.getInt(1);
            }
            String sql3="";
            PreparedStatement stmt3=null;
            if(user instanceof Student){
                sql3= "insert into student values (?, ?)";
                stmt3 = connection.prepareStatement(sql3);
                stmt3.setInt(1, id);
                stmt3.setInt(2, ((Student)user).getCourse());
            }
            if(user instanceof Teacher){
                sql3= "insert into teacher values (?, ?)";
                stmt3 = connection.prepareStatement(sql3);
                stmt3.setInt(1, id);
                stmt3.setString(2, ((Teacher)user).getPost());
            }
            stmt3.executeUpdate();
            connection.close();
        } catch (Exception e) {
                System.out.println("Ошибка регистрации!");
                JOptionPane.showMessageDialog(null, "Данный пользователь уже существует!");
                return;
        }
    }

    public static void addNewBook(Book book, int id){
        try {
            Connection connection = connectToDB();
            String sql = "INSERT INTO book (id, title, author, subject, paragraphs, url) values(?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.setString(2, book.getTitle());
            stmt.setString(3, book.getAuthor());
            stmt.setString(4, book.getSubject());
            stmt.setString(5, book.getThemes());
            stmt.setString(6, book.getURL());
            stmt.executeUpdate();
            stmt.close();
            connection.close();
            JOptionPane.showMessageDialog(null, "Учебник успешно добавлен!");
        }
        catch (Exception e){
            return;
        }
    }

    public static ArrayList<Book> getAllBooks(int id){
        ArrayList<Book> books=new ArrayList<>();
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs;
            if(id==0)
                rs=stmt.executeQuery("select * from book");
            else
                rs = stmt.executeQuery("select * from book where id=" + id);
            while (rs.next()){
                Book book=new Book();
                book.setTitle(rs.getString("title"));
                book.setAuthor(rs.getString("author"));
                book.setSubject(rs.getString("subject"));
                book.setThemes(rs.getString("paragraphs"));
                book.setURL(rs.getString("url"));
                books.add(book);
            }
            return books;
        }
        catch (Exception e){
            return null;
        }
    }

    public static int findId(Book book){
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs=stmt.executeQuery("select id from book where title='"+book.getTitle()+"' and author='"+book.getAuthor()+"' and subject='"+book.getSubject()+"' and paragraphs='"+book.getThemes()+"' and url='"+book.getURL()+"'");
            while (rs.next()){
                return rs.getInt(1);
            }
            return 0;
        }
        catch (Exception e){
            return 0;
        }
    }

    public static ArrayList<Book> getRecomendedBooks(){
        ArrayList<Book> books=new ArrayList<>();
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from book");
            while (rs.next()){
                Statement stmt2=connection.createStatement();
                ResultSet rs2=stmt2.executeQuery("select * from teacher where id="+rs.getInt("id"));
                if(rs2.next()==true){
                Book book=new Book();
                    book.setTitle(rs.getString("title"));
                    book.setAuthor(rs.getString("author"));
                    book.setSubject(rs.getString("subject"));
                    book.setThemes(rs.getString("paragraphs"));
                    book.setURL(rs.getString("url"));
                    books.add(book);
                }
            }
            return books;
        }
        catch (Exception e){
            return null;
        }
    }

    public static void deleteBook(int id, Book book){
        try {
            Connection connection = connectToDB();
            String sql;
            if(id!=0)
                sql = "Delete from book where id=? and title=? and author=? and subject=? and paragraphs=? and url=?";
            else
                sql = "Delete from book where title=? and author=? and subject=? and paragraphs=? and url=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            if(id!=0) {
                stmt.setInt(1, id);
                stmt.setString(2, book.getTitle());
                stmt.setString(3, book.getAuthor());
                stmt.setString(4, book.getSubject());
                stmt.setString(5, book.getThemes());
                stmt.setString(6, book.getURL());
            }
            else{
                stmt.setString(1, book.getTitle());
                stmt.setString(2, book.getAuthor());
                stmt.setString(3, book.getSubject());
                stmt.setString(4, book.getThemes());
                stmt.setString(5, book.getURL());
            }
            stmt.executeUpdate();
            stmt.close();
            connection.close();
        }
        catch (Exception e){
            return;
        }
    }

    public static ArrayList<Teacher> getTeachers(){
        ArrayList<Teacher> teachers=new ArrayList<>();
        try {
            Connection connection = connectToDB();
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select * from userdata where confirmed=0");
            while (rs.next()){
                Teacher teacher=new Teacher();
                teacher.setLastName(rs.getString("lastname"));
                teacher.setFirstName(rs.getString("firstname"));
                teacher.setPatronymic(rs.getString("patronymic"));
                teacher.setId(rs.getInt("id"));
                Statement stmt2 = connection.createStatement();
                ResultSet rs2 = stmt2.executeQuery("select * from teacher where id="+teacher.getId());
                rs2.next();
                teacher.setPost(rs2.getString("post"));
                teachers.add(teacher);
            }
            return teachers;
        }
        catch (Exception e){
            return null;
        }
    }

    public static void acceptTeacher(Teacher teacher){
        try {
            Connection connection = connectToDB();
            String sql = "update userdata set confirmed=1 where lastname=? and firstname=? and patronymic=? and id=?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, teacher.getLastName());
            stmt.setString(2, teacher.getFirstName());
            stmt.setString(3, teacher.getPatronymic());
            stmt.setInt(4, teacher.getId());
            stmt.executeUpdate();
            stmt.close();
            connection.close();
        }
        catch (Exception e){
            return;
        }
    }
}