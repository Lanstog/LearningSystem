package Models;

public class Book {
    private String title;
    private String author;
    private String subject;
    private String themes;
    private String URL;

    public String getAuthor() {
        return author;
    }

    public String getSubject() {
        return subject;
    }

    public String getThemes() {
        return themes;
    }

    public String getTitle() {
        return title;
    }

    public String getURL() {
        return URL;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setThemes(String themes) {
        this.themes = themes;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
